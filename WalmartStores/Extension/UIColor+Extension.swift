//
//  UIColor+Extension.swift
//  WalmartStores
//
//  Created by Michel Franco on 7/9/19.
//  Copyright © 2019 Michel Franco. All rights reserved.
//

import UIKit

extension UIColor {
    static var primary: UIColor {
        return UIColor(named: "Primary") ?? UIColor.blue
    }
}
