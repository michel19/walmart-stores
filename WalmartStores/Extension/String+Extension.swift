//
//  String+Extension.swift
//  WalmartStores
//
//  Created by Michel Franco on 7/9/19.
//  Copyright © 2019 Michel Franco. All rights reserved.
//

import Foundation

extension String {
    var toError: Error {
        let error = NSError(domain: "", code: -1, userInfo: [NSLocalizedDescriptionKey: self]) as Error
        return error
    }
    
    var scheduleValue: String {
        let scheduleArray = self.components(separatedBy: "/")
        return scheduleArray.first ?? self
    }
    
    var firstPhone: String {
        let phones = self.components(separatedBy: "/")
        return phones.first ?? self
    }
}
