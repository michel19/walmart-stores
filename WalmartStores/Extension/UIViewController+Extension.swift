//
//  UIViewController+Extension.swift
//  WalmartStores
//
//  Created by Michel Franco on 7/10/19.
//  Copyright © 2019 Michel Franco. All rights reserved.
//

import UIKit

extension UIViewController {
    /// Shows error alert with a single `ok` button
    ///
    /// - Parameters:
    ///   - title: Alert title.
    ///   - errorMessage: Alert message.
    func showAlertWith(title: String?, andErrorMessage errorMessage: String) {
        let alert = UIAlertController(title: title, message: errorMessage, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
        
        alert.addAction(action)
        
        self.present(alert, animated: true, completion: nil)
    }
}
