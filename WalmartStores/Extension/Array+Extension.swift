//
//  Array+Extension.swift
//  WalmartStores
//
//  Created by Michel Franco on 7/10/19.
//  Copyright © 2019 Michel Franco. All rights reserved.
//

import CoreLocation

extension Array where Element == Store {
    
    internal func get(first: Int, storesNearTo location: CLLocationCoordinate2D) -> [Store] {
        let currentLocation = CLLocation(latitude: location.latitude, longitude: location.longitude)
        let sorted = self.sorted { (store1, store2) -> Bool in
            var _store1 = store1, _store2 = store2
            
            guard let location1 = _store1.location, let location2 = _store2.location else { return false }
            
            let distance1 = CLLocation(latitude: location1.latitude, longitude: location1.longitude).distance(from: currentLocation)
            let distance2 = CLLocation(latitude: location2.latitude, longitude: location2.longitude).distance(from: currentLocation)
            
            return distance1 < distance2
        }.prefix(first)
        
        return Array(sorted)
    }
    
}
