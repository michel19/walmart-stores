//
//  LocatorRepository.swift
//  WalmartStores
//
//  Created by Michel Franco on 7/9/19.
//  Copyright © 2019 Michel Franco. All rights reserved.
//

import Alamofire

class LocatorRepository {
    
    private let storeLocatorPath = "walmart-services/mg/address/storeLocatorCoordinates"
    
    func getStoresRequest() -> DataRequest? {
        return Repository.shared.getRequest(method: .get, path: storeLocatorPath)
    }
    
}
