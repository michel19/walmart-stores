//
//  StoreTableViewCell.swift
//  WalmartStores
//
//  Created by Michel Franco on 7/9/19.
//  Copyright © 2019 Michel Franco. All rights reserved.
//

import UIKit

class StoreTableViewCell: UITableViewCell {

    @IBOutlet weak var imgStore: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSchedule: UILabel!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    @IBOutlet weak var imgDisclosure: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        startSkelleton()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    private func startSkelleton() {
        imgStore.showAnimatedGradientSkeleton()
        lblTitle.showAnimatedGradientSkeleton()
        lblSchedule.showAnimatedGradientSkeleton()
        lblPhoneNumber.showAnimatedGradientSkeleton()
        imgDisclosure.showAnimatedGradientSkeleton()
    }
    
    func stopSkeleton() {
        imgStore.hideSkeleton()
        lblTitle.hideSkeleton()
        lblSchedule.hideSkeleton()
        lblPhoneNumber.hideSkeleton()
        imgDisclosure.hideSkeleton()
    }
    
}
