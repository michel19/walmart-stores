//
//  LocatorViewController.swift
//  WalmartStores
//
//  Created by Michel Franco on 7/9/19.
//  Copyright © 2019 Michel Franco. All rights reserved.
//

import UIKit
import GoogleMaps
import SkeletonView
import CoreLocation

protocol LocatorUI {
    func showLoaderIndicator()
    func display(stores: [Store])
    func display(error: Error)
}

class LocatorViewController: UIViewController {

    @IBOutlet weak var gmView: GMSMapView!
    @IBOutlet weak var btnLocalize: UIButton!
    @IBOutlet weak var tvStores: UITableView!
    
    fileprivate let cellReuseIdentifier = "StoreCell"
    
    private let locationManager = CLLocationManager()
    private var stores: [Store]?
    private var interactor: LocatorInteractor?
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setUp()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }
    
    private func setUp() {
        let view = self
        let interactor = LocatorInteractorImpl()
        let presenter = LocatorPresenterImpl()
        interactor.presenter = presenter
        presenter.ui = view
        view.interactor = interactor
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
        setUpDelegates()
        locationManager.requestWhenInUseAuthorization()
    }
    
    private func initUI() {
        title = "Localizador"
        tvStores.register(UINib(nibName: "StoreTableViewCell", bundle: nil), forCellReuseIdentifier: cellReuseIdentifier)
        initNavBar()
    }
    
    private func setUpDelegates() {
        tvStores.delegate = self
        tvStores.dataSource = self
        locationManager.delegate = self
    }
    
    private func initNavBar() {
        self.navigationController?.navigationBar.barTintColor = UIColor.primary
        self.navigationController?.navigationBar.barStyle = .black
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    
    @IBAction func onBtnLocateme(_ sender: UIButton) {
        locationManager.startUpdatingLocation()
    }
    
    fileprivate func showLocationAlert() {
        let alert = UIAlertController(title: "Error", message: "Por favor permite el acceso a tu ubicación para brindarte una mejor experiencia", preferredStyle: .alert)
        let actionOk = UIAlertAction(title: "Ok", style: .default, handler: nil)
        let actionOpen = UIAlertAction(title: "Abrir Settings", style: .default) { _ in
            if let url = URL.init(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        
        alert.addAction(actionOk)
        alert.addAction(actionOpen)
        
        present(alert, animated: true, completion: nil)
    }
}

extension LocatorViewController: SkeletonTableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stores?.count ?? 8
    }
    
    func numSections(in collectionSkeletonView: UITableView) -> Int {
        return 1
    }
    
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return cellReuseIdentifier
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as? StoreTableViewCell else {
            return UITableViewCell()
        }
        
        if let store = stores?[indexPath.row] {
            cell.stopSkeleton()
            cell.lblTitle.text = store.name
            cell.lblSchedule.text = "Horario: \(store.schedule?.scheduleValue ?? "")"
            cell.lblPhoneNumber.text = "Teléfono: \(store.telephones?.firstPhone ?? "")"
        }
        
        return cell
    }
    
}

extension LocatorViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if stores != nil {
            let storeVC = StoreDetailViewController()
            var store = stores?[indexPath.row]
            storeVC.store = store
            var tmpStores = stores
            tmpStores?.remove(at: indexPath.row)
            if let nearStores = tmpStores, let location = store?.location {
                storeVC.nearStores = nearStores.get(first: 4, storesNearTo: location)
            }
            navigationController?.pushViewController(storeVC, animated: true)
        }
    }
}

extension LocatorViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        guard status == .authorizedWhenInUse else {
            if status == .denied {
                showLocationAlert()
            }
            return
        }
        locationManager.startUpdatingLocation()
        gmView.isMyLocationEnabled = true
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else { return }
        
        gmView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
        locationManager.stopUpdatingLocation()
        
        interactor?.getStores(for: location.coordinate)
    }
}

extension LocatorViewController: LocatorUI {
    func showLoaderIndicator() {
        stores = nil
        tvStores.reloadData()
    }
    
    func display(stores: [Store]) {
        self.stores = stores
        tvStores.reloadData()
    }
    
    func display(error: Error) {
        showAlertWith(title: "Error", andErrorMessage: error.localizedDescription)
    }
    
}
