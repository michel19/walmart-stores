//
//  LocatorPresenter.swift
//  WalmartStores
//
//  Created by Michel Franco on 7/9/19.
//  Copyright © 2019 Michel Franco. All rights reserved.
//

import Foundation
import CoreLocation

protocol LocatorPresenter: class {
    func displayStoresResult(response: StoresResponse?, error: Error?, location: CLLocationCoordinate2D)
}

class LocatorPresenterImpl: LocatorPresenter {
    
    var ui: LocatorUI?
    
    func displayStoresResult(response: StoresResponse?, error: Error?, location: CLLocationCoordinate2D) {
        ui?.showLoaderIndicator()
        
        if let _stores = response?.stores, !_stores.isEmpty {
            let filteredStores = _stores.get(first: 25, storesNearTo: location)
            ui?.display(stores: filteredStores)
        } else if let _error = error {
            ui?.display(error: _error)
        } else {
            ui?.display(error: "Tenemos un problema de comunicacion, por favor inténtalo más tarde.".toError)
        }
        
    }
}
