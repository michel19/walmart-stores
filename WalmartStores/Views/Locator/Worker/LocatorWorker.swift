//
//  LocatorWorker.swift
//  WalmartStores
//
//  Created by Michel Franco on 7/9/19.
//  Copyright © 2019 Michel Franco. All rights reserved.
//

import Foundation

typealias SuccessStores = (_ response: StoresResponse) -> Void
typealias FailureStores = (_ error: Error) -> Void

class Worker {
    let repository = LocatorRepository()
    
    func getStores(success: @escaping SuccessStores, failure: @escaping FailureStores) {
        guard let request = repository.getStoresRequest() else {
            failure("Tenemos un problema de comunicaciones".toError)
            return
        }
        
        Repository.shared.processRequestIntoResponse(request: request, model: StoresResponse.self, success: { (response) in
            success(response)
        }) { (error) in
            failure(error)
        }
    }
}
