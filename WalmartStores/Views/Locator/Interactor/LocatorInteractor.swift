//
//  LocatorInteractor.swift
//  WalmartStores
//
//  Created by Michel Franco on 7/9/19.
//  Copyright © 2019 Michel Franco. All rights reserved.
//

import Foundation
import CoreLocation

protocol LocatorInteractor: class {
    func getStores(for location: CLLocationCoordinate2D)
}

class LocatorInteractorImpl: LocatorInteractor {
    
    var presenter: LocatorPresenter?
    private let worker = Worker()
    
    func getStores(for location: CLLocationCoordinate2D) {
        worker.getStores(success: { (response) in
            self.presenter?.displayStoresResult(response: response, error: nil, location: location)
        }) { (error) in
            self.presenter?.displayStoresResult(response: nil, error: error, location: location)
        }
    }
    
}
