//
//  Store.swift
//  WalmartStores
//
//  Created by Michel Franco on 7/9/19.
//  Copyright © 2019 Michel Franco. All rights reserved.
//

import ObjectMapper
import CoreLocation

struct Store {
    
    var id: Int?
    var storeID: String?
    var name: String?
    var address: String?
    var telephones: String?
    var manager: String?
    var zipCode: String?
    var latitude: CLLocationDegrees?
    var longitude: CLLocationDegrees?
    var schedule: String?
    
    lazy var location: CLLocationCoordinate2D? = {
        guard let lat = latitude, let lng = longitude else { return nil }
        return CLLocationCoordinate2D(latitude: lat, longitude: lng)
    }()
    
    init?(map: Map) { }
    
}

extension Store: Mappable {
    mutating func mapping(map: Map) {
        id              <- map["id"]
        storeID         <- map["storeID"]
        name            <- map["name"]
        address         <- map["address"]
        telephones      <- map["telephone"]
        manager         <- map["manager"]
        zipCode         <- map["zipCode"]
        latitude        <- (map["latPoint"], StringToCLLocationDegreeTransform())
        longitude       <- (map["lonPoint"], StringToCLLocationDegreeTransform())
        schedule        <- map["opens"]
    }
}
