//
//  StoresResponse.swift
//  WalmartStores
//
//  Created by Michel Franco on 7/9/19.
//  Copyright © 2019 Michel Franco. All rights reserved.
//

import ObjectMapper

struct StoresResponse {
    
    var codeMessage: Int?
    var message: String?
    var stores: [Store]?
    
    init?(map: Map) { }
    
}

extension StoresResponse: Mappable {
    mutating func mapping(map: Map) {
        codeMessage         <- map["codeMessage"]
        message             <- map["message"]
        stores              <- map["responseArray"]
    }
}
