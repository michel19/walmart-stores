//
//  StoreDetailViewController.swift
//  WalmartStores
//
//  Created by Michel Franco on 7/10/19.
//  Copyright © 2019 Michel Franco. All rights reserved.
//

import UIKit
import GoogleMaps

class StoreDetailViewController: UIViewController {
    
    @IBOutlet weak var gmViews: GMSMapView!
    @IBOutlet weak var tvNearStores: UITableView!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblSchedule: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblManager: UILabel!
    
    fileprivate let cellReuseIdentifier = "NearStoreCell"
    
    var store: Store?
    var nearStores: [Store]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
        tvNearStores.register(UITableViewCell.self, forCellReuseIdentifier: cellReuseIdentifier)
    }
    
    private func initUI() {
        title = store?.name?.capitalized ?? "Tienda"
        lblAddress.text = store?.address?.capitalized
        lblSchedule.text = store?.schedule?.scheduleValue
        lblPhone.text = store?.telephones
        lblManager.text = store?.manager?.capitalized
        
        initMap()
    }
    
    private func initMap() {
        guard let storeLocation = store?.location else { return }
        gmViews.camera = GMSCameraPosition(target: storeLocation, zoom: 15, bearing: 0, viewingAngle: 0)
        setMarker(in: storeLocation, title: store?.name?.capitalized ?? "Tienda")
        nearStores?.forEach({ (store) in
            var _store = store
            guard let location = _store.location, let title = _store.toJSONString() else { return }
            setMarker(in: location, title: title)
        })
    }
    
    private func setMarker(in location: CLLocationCoordinate2D, title: String) {
        let marker = GMSMarker()
        marker.icon = UIImage(named: "Pin")
        marker.position = location
        marker.title = title
        marker.map = gmViews
    }

    private func moveCamera(to location: CLLocationCoordinate2D) {
         gmViews.camera = GMSCameraPosition(target: location, zoom: 15, bearing: 0, viewingAngle: 0)
    }
    
}

extension StoreDetailViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nearStores?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath)
        
        cell.accessoryType = .disclosureIndicator
        cell.selectionStyle = .none
        cell.textLabel?.text = nearStores?[indexPath.row].name
        
        return cell
    }
    
}

extension StoreDetailViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard var nearStore = nearStores?[indexPath.row], let location = nearStore.location else { return }
        moveCamera(to: location)
    }
}
