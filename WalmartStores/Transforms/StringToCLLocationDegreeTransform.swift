//
//  StringToCLDegreTransform.swift
//  WalmartStores
//
//  Created by Michel Franco on 7/10/19.
//  Copyright © 2019 Michel Franco. All rights reserved.
//

import ObjectMapper
import CoreLocation

class StringToCLLocationDegreeTransform: TransformType {
    typealias Object = CLLocationDegrees
    typealias JSON = String
    
    func transformFromJSON(_ value: Any?) -> CLLocationDegrees? {
        guard let _value = value as? String,
        let doubleDegree = Double(_value) else { return nil }
        return CLLocationDegrees(doubleDegree)
    }
    
    func transformToJSON(_ value: CLLocationDegrees?) -> String? {
        guard let _value = value else { return nil }
        return "\(_value)"
    }
    
}
