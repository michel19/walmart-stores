//
//  Repository.swift
//  WalmartStores
//
//  Created by Michel Franco on 7/9/19.
//  Copyright © 2019 Michel Franco. All rights reserved.
//

import Alamofire
import ObjectMapper

class Repository {
    static let shared = Repository()
    
    private let config = ServiceConfig()
    
    /// Get data request object to operate and get http response.
    ///
    /// - Parameters:
    ///   - method: Http method to do request.
    ///   - path: path where service or resource is located.
    /// - Returns: data request object.
     func getRequest(method: Alamofire.HTTPMethod, path: String) -> DataRequest? {
        guard let _config = config else { return nil }
        let service = Service(configuration: _config)
        let request = Request(method: method, path: path, parameters: nil, encoding: URLEncoding.default, headers: nil)
        return service.excecute(request: request)
    }
    
    /// Response model that extends from Mappable protocol.
    ///
    /// - Parameters:
    ///   - model: Response model type.
    ///   - response: String response result of service call.
    /// - Returns: Response model object.
    private func getResponse<T: Mappable>(model: T.Type, response: String) -> T? {
        return T(JSONString: response)
    }
    
    /// Proccess Http request and transforms result into an object model.
    ///
    /// - Parameters:
    ///   - request: Http request object.
    ///   - model: Model type of response.
    ///   - success: Success closure in where we can get object response model.
    ///   - failure: Failure closure with error object.
    func processRequestIntoResponse<T: Mappable>(request: DataRequest, model: T.Type, success: @escaping (_ response: T) -> Void, failure: @escaping (_ error: Error) -> Void) {
        request.responseString { response in
            switch response.result {
            case .success(let value):
                guard let responseModel = self.getResponse(model: model.self, response: value) else {
                    failure("Error parsing response into model".toError)
                    return
                }
                success(responseModel)
            case .failure(let error):
                failure(error)
            }
        }
    }
    
}
