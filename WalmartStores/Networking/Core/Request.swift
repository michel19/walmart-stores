//
//  Request.swift
//  WalmartStores
//
//  Created by Michel Franco on 7/9/19.
//  Copyright © 2019 Michel Franco. All rights reserved.
//

import Alamofire

class Request {
    
    public var method: Alamofire.HTTPMethod
    public var path: String
    public var parameters: [String: Any]?
    public var encoding: ParameterEncoding = URLEncoding.default
    public var headers: [String: String]?
    
    /// Service request object data.
    ///
    /// - Parameters:
    ///   - method: Http method object.
    ///   - path: Service path.
    ///   - parameters: Service dictionary parametters.
    ///   - encoding: URL encoding type.
    ///   - headers: Http dictionary headers.
    required init(method: Alamofire.HTTPMethod, path: String, parameters: [String: Any]? = nil, encoding: ParameterEncoding = URLEncoding.default, headers: [String: String]? = nil) {
        self.method = method
        self.path = path
        self.parameters = parameters
        self.encoding = encoding
        self.headers = headers
    }
    
}
