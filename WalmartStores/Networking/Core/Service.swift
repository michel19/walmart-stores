//
//  Service.swift
//  WalmartStores
//
//  Created by Michel Franco on 7/9/19.
//  Copyright © 2019 Michel Franco. All rights reserved.
//

import Alamofire

public final class Service {
    private var configuration: ServiceConfig
    
    /// Returns Service object.
    ///
    /// - Parameter configuration: Service configuration with endpoint info.
    init(configuration: ServiceConfig) {
        self.configuration = configuration
    }
    
    /// Excecutes service request and creates
    /// an observable object to mapping response.
    ///
    /// - Parameter request: Service request object.
    /// - Returns: Observable object with http response data.
    func excecute(request: Request) -> DataRequest {
        let url = "\(configuration.url.absoluteString)/\(request.path)"
        return Alamofire.request(url, method: request.method, parameters: request.parameters, encoding: request.encoding, headers: configuration.headers)
    }
    
}
