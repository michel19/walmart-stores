//
//  ServiceConfiguration.swift
//  WalmartStores
//
//  Created by Michel Franco on 7/9/19.
//  Copyright © 2019 Michel Franco. All rights reserved.
//

import Foundation

public final class ServiceConfig: CustomStringConvertible {
    
    private static let ENDPOINT = "Endpoint"
    private static let NAME = "name"
    private static let URL_ = "url"
    private static let HEADERS = "headers"
    
    public var description: String {
        return "\(self.name): \(self.url.absoluteString)"
    }
    
    private(set) var name: String
    private(set) var url: URL
    private(set) var headers: [String: String] = [:]
    
    public var cachePolicy: URLRequest.CachePolicy = .useProtocolCachePolicy
    public var timeout: TimeInterval = 15.0
    
    /// Initializer for custom endpoint.
    ///
    /// - Parameters:
    ///   - name: Endpoint interpretable name.
    ///   - url: Endpoint url text representation.
    public init?(name: String, url: String) {
        guard let _url = URL(string: url) else { return nil }
        self.url = _url
        self.name = name
    }
    
    /// Initializer for global Endpoint config
    /// defined in `Info.plist` file
    public convenience init?() {
        let appConfig = Bundle.main.object(forInfoDictionaryKey: ServiceConfig.ENDPOINT)
        guard let url = (appConfig as? [String: Any])?[ServiceConfig.URL_] as? String,
            let name = (appConfig as? [String: Any])?[ServiceConfig.NAME] as? String else {
                return nil
        }
        
        self.init(name: name, url: url)
        
        if let headers = (appConfig as? [String: Any])?[ServiceConfig.HEADERS] as? [String: String] {
            self.headers = headers
        }
    }
    
    /// Shared ServiceConfig instance
    ///
    /// - Returns: ServiceConfig object
    public static func appConfig() -> ServiceConfig? {
        return ServiceConfig()
    }
    
}
